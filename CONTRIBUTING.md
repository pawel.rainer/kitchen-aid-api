# Contribution

## Commit messages

Commit messages should follow <https://www.conventionalcommits.org/> convention.

### Commit types:

| Type     | Description           | Example                       |
|----------|-----------------------|-------------------------------|
| feat     | new feature           | feat: implement xxx           |
| fix      | bug fix               | fix: xxx                      |
| refactor | code refactor         | refactor: xxx                 |
| test     | add missing tests     | test: xxx                     |
| style    | prettify code         | style: optimize imports       |
| docs     | documenation changes  | docs: update readme           |
| chore    | build process changes | chore: add xxx job to pipline |